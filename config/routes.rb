Rails.application.routes.draw do

  root 'home#index'

  # Routes to leave
  get 'leaves/index' => 'leaves#index', :as => 'leaves_index'
  get 'leaves/show/:id' => 'leaves#show', :as => 'leaves_show'
  get 'leaves/new' => 'leaves#new', :as => 'leaves_new'
  get 'leaves/create_sector' => 'leaves#create_sector', :as => 'leaves_create_sector'
  post 'leaves/create' => 'leaves#create', :as => 'leaves_create'

  # Routes to entry
  get 'entries/index' => 'entries#index', :as => 'entries_index'
  get 'entries/show/:id' => 'entries#show', :as => 'entries_show'
  get 'entries/new' => 'entries#new', :as => 'entries_new'
  post 'entries/create' => 'entries#create', :as => 'entries_create'
  get 'entries/receive/:id' => 'entries#receive', :as => 'entries_receive'
  post 'entries/update/:id' => 'entries#update', :as => 'entries_update'

  # Routes to material
  get 'materials/index' => 'materials#index', :as => 'materials_index'
  get 'materials/new' => 'materials#new', :as => 'materials_new'
  post 'materials/create' => 'materials#create', :as => 'materials_create'
  get 'materials/edit/:id' => 'materials#edit', :as => 'materials_edit'
  patch 'materials/update/:id' => 'materials#update', :as => 'materials_update'
  get 'materials/destroy/:id' => 'materials#destroy', :as => 'materials_destroy'

  # Routes to user
  get 'users/index' => 'users#index', :as => 'users_index'
  get 'users/active/:id' => 'users#active', :as => 'users_active'
  get 'users/new' => 'users#new', :as => 'users_new'
  post 'users/create' => 'users#create', :as => 'users_create'
  get 'users/edit' => 'users#edit', :as => 'users_edit'
  patch 'users/update' => 'users#update', :as => 'users_update'
  get 'users/destroy' => 'users#destroy', :as => 'users_destroy'

  # Session routs
  get 'log_out' => 'sessions#destroy', :as => 'log_out'
  get 'log_in' => 'sessions#new', :as => 'log_in'
  post 'init_session' => 'sessions#create', :as => 'sessions_create'

  # home page route
  get 'home/index' => 'home#index', :as => 'home'
end
