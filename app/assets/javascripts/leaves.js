$(document).on('turbolinks:load', function() {
  $("#save_sector").on("click", function(){
    $.getJSON( "create_sector?sector_name=" + $("#sector_name").val(),
    { format: 'json'
    }).success(function(data){
      $('#leave_sector').empty();
      $.each(data, function(i, item){
  			var option = "<option value='"+item.id+"'>"+item.name+"</option>";
  			$('#leave_sector').append(option);
  		})
      $("#sector_name").val('')
    }).error(function(){
      alert("Falha no Processamento");
    });
  });
});
