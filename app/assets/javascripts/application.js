// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require bootstrap-sprockets
//= require_tree .

$(document).on('turbolinks:load', function() {
  var material_counter = 1;
  $('#new_material').on('click', function() {
    //Adding materials name field
    $('#new_material_list').append('<label for="material">Material ' + (material_counter+1) +':</label><br />');
    $('#new_material_list').append('<select name="material[id][' + material_counter + ']" id="material_id_' + material_counter + '"> </select><br />');

    //Adding material amount field
    $('#new_material_list').append('<label for="amount">Quantidade ' + (material_counter+1) +':</label><br />');
    $('#new_material_list').append(' <input type="number" name="material[amount][' + material_counter + ']" id="material_amount_' + material_counter + '" /> <br /><br />');

    // fill select with materials
    var $options = $("#material_id_0 > option").clone();
    $('#material_id_' + (material_counter++)).append($options);
  });

});
