# Base class with the methods used by most views
module ApplicationHelper
  def flash_message
    messages = ''
    %i[notice info warning error].each do |type|
      messages += flash[type].to_s
    end
    messages
  end
end
