# Base class with the users methods used by views
module UsersHelper
  def total_users
    User.all.size
  end
end
