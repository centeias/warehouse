class MaterialLeave < ApplicationRecord
  belongs_to :material
  belongs_to :leave

  # Exception messages
  INVALID_RECEIVE_AMOUNT = 'Quantidade recebida deve ser preenchida'.freeze
  RECEIVE_AMOUNT_IS_NUMERIC = 'Quantidade recebida deve ser numérica'.freeze
  INVALID_MATERIAL = 'Material deve ser preenchido'.freeze
  INVALID_ENTRY = 'Dados da entrada devem ser preenchidos'.freeze

  # Validates
  validates :amount_received,
            presence: { message: INVALID_RECEIVE_AMOUNT }
  validates :amount_received,
            numericality: { message: RECEIVE_AMOUNT_IS_NUMERIC }

  validates :material, presence: { message: INVALID_MATERIAL }
  validates :leave, presence: { message: INVALID_ENTRY }
end
