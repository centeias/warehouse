class MaterialEntry < ApplicationRecord
  belongs_to :entry
  belongs_to :material

  # Exception messages
  INVALID_REQUESTED_AMOUNT = 'Quantidade solicitada deve ser preenchida'.freeze
  REQUESTED_AMOUNT_IS_NUMERIC = 'Quantidade solicitada deve ser numérica'.freeze
  INVALID_RECEIVE_AMOUNT = 'Quantidade recebida deve ser preenchida'.freeze
  RECEIVE_AMOUNT_IS_NUMERIC = 'Quantidade recebida deve ser numérica'.freeze
  INVALID_MATERIAL = 'Material deve ser preenchido'.freeze
  INVALID_ENTRY = 'Dados da entrada devem ser preenchidos'.freeze

  # Validates
  validates :requested_amount, presence: { message: INVALID_REQUESTED_AMOUNT }
  validates :requested_amount,
            numericality: { message: REQUESTED_AMOUNT_IS_NUMERIC }

  validates :amount_received,
            presence: { message: INVALID_RECEIVE_AMOUNT }, on: :update
  validates :amount_received,
            numericality: { message: RECEIVE_AMOUNT_IS_NUMERIC }, on: :update

  validates :material, presence: { message: INVALID_MATERIAL }
  validates :entry, presence: { message: INVALID_ENTRY }
end
