class RequestingSector < ApplicationRecord
  has_many :leave

  # Exception messages
  INVALID_NAME = 'Nome não pode ser nulo'.freeze

  validates :name, presence: { message: INVALID_NAME }
end
