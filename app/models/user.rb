class User < ApplicationRecord
  attr_accessor :password
  before_save :encrypt_password

  # Exception messages
  DIFERENT_PASSWORDS = 'Senhas Diferem'.freeze
  PASSWORD_CANT_BLANCK = 'Preencha a Senha'.freeze
  EMAIL_CANT_BLANCK = 'Preencha o Email'.freeze
  EMAIL_CAN_EXISTS = 'Email já cadastrado'.freeze

  # Validates
  validates :password, confirmation: { message: DIFERENT_PASSWORDS }
  validates :password, presence: { message: PASSWORD_CANT_BLANCK }, on: :create
  validates :email, presence: { message: EMAIL_CANT_BLANCK }
  validates :email, uniqueness: { message: EMAIL_CAN_EXISTS }

  # Model method to check user authenticate
  def self.authenticate(email, password)
    user = find_by_email(email)
    if user &&
       user.password_hash == BCrypt::Engine.hash_secret(password,
                                                        user.password_salt)
      user
    end
  end

  def encrypt_password
    return unless password.present?
    self.password_salt = BCrypt::Engine.generate_salt
    self.password_hash = BCrypt::Engine.hash_secret(password, password_salt)
  end
end
