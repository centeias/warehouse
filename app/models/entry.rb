class Entry < ApplicationRecord
  belongs_to :requested_user, class_name: 'User'
  belongs_to :user_receive, class_name: 'User', optional: true
  has_many :material_entry, autosave: true

  # Exception messages
  INVALID_USER = 'Usuário não pode ser nulo'.freeze
  INVALID_DATE = 'Data não pode ser nula '.freeze

  # Validates
  validates :requested_user, presence: { message: INVALID_USER }
  validates :requested_date, presence: { message: INVALID_DATE }
  validates :user_receive, presence: { message: INVALID_USER }, on: :update
  validates :receive_date, presence: { message: INVALID_DATE }, on: :update

  validate :validates_material_entry, on: :update

  def validates_material_entry
    material_entry.each do |material|
      if material.amount_received.nil?
        errors.add(:material_entry, 'Quantidade recebida deve ser preenchida')
        break
      end
    end
  end
end
