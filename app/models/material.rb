class Material < ApplicationRecord
  has_many :material_entry
  has_many :material_leave

  # Exception messages
  INVALID_NAME = 'Preencha o nome do Produto'.freeze
  INVALID_AMOUNT = 'Preencha a quantidade inicial'.freeze
  AMOUT_IS_NUMERIC = 'Quantidade deve ser numérico'.freeze

  # Validates
  validates :name, presence: { message: INVALID_NAME }
  validates :initial_amount, presence: { message: INVALID_AMOUNT }, on: :create
  validates :initial_amount, numericality: { message: AMOUT_IS_NUMERIC }

  def total_amount
    total = initial_amount

    material_entry.each do |entry|
      total += entry.amount_received
    end

    material_leave.each do |leave|
      total -= leave.amount_received
    end
    total
  end
end
