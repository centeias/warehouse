class Leave < ApplicationRecord
  belongs_to :employee, class_name: 'User'
  has_many :material_leave, autosave: true
  belongs_to :requesting_sector

  # Exception messages
  INVALID_USER = 'Usuário não pode ser nulo'.freeze
  INVALID_PERSON = 'Solicitante não pode ser nulo'.freeze
  INVALID_SECTOR = 'Setor solicitante não pode ser nulo'.freeze
  INVALID_DATE = 'Data não pode ser nula '.freeze

  # Validates
  validates :employee, presence: { message: INVALID_USER }
  validates :requested_date, presence: { message: INVALID_DATE }
  validates :requested_person, presence: { message: INVALID_PERSON }
  validates :requesting_sector, presence: { message: INVALID_SECTOR }
end
