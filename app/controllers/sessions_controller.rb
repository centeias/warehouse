# Manager of session user
class SessionsController < ApplicationController
  def new
    # loggin page
  end

  def create
    user = User.authenticate(params[:email], params[:password])
    if user && !user.active.zero?
      session[:user_id] = user.id
      redirect_to home_path, flash: { notice: 'Logged in!' }
    else
      invalid_user(user)
      render :new
    end
  end

  def destroy
    session[:user_id] = nil
    redirect_to root_url, flash: { notice: 'Logged out!' }
  end

  private

  def invalid_user(user)
    flash[:error] = if user && user.active.zero?
                      'Usuário desativado'
                    else
                      'Usuário e/ou senha inválidos'
                    end
  end
end
