# controller class to manager material models, only Create, Update and View
class MaterialsController < ApplicationController
  def index
    @materials = Material.all
  end

  def new
    @material = Material.new
  end

  def create
    @material = Material.new(product_params_new)
    if @material.save
      message = 'Cadastrado com sucesso!'
      redirect_to materials_index_path, flash: { notice: message }
    else
      ocurred_errors(@material)
      render :new
    end
  end

  def edit
    @material = Material.find(params[:id])
  end

  def update
    @material = Material.find(params[:id])
    if @material.update(product_params_update)
      message = 'Dados atualizados com sucesso'
      redirect_to materials_index_path, flash: { success: message }
    else
      ocurred_errors(@material)
      render :edit
    end
  end

  private

  def product_params_update
    params[:material].permit(:name)
  end

  def product_params_new
    params[:material].permit(:name, :initial_amount)
  end
end
