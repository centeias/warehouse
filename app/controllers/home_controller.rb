# Simple controller with dashboard with warehouse situation
class HomeController < ApplicationController
  before_action :autenticated?

  def index
    # home page
    @materials = Material.all
  end
end
