# Base class with the methods used by most classes
class ApplicationController < ActionController::Base
  # using method in controller and views
  helper_method :current_user

  def ocurred_errors(object)
    message = ''
    object.errors.messages.each do |_attrib, messages|
      message += ', ' if message != ''
      message += messages.join(', ')
    end
    flash[:error] = message
  end

  private

  def current_user
    User.find(session[:user_id]) unless session[:user_id].nil?
  end

  def autenticated?
    return unless current_user.nil?

    flash.now[:notice] = 'Você precisa estar logado'
    redirect_to log_in_path
  end
end
