# controller class to manager entry and entry_material models, only:
# Create, Update and View
class EntriesController < ApplicationController
  def index
    @entries = Entry.all
  end

  def show
    @entry = Entry.find(params[:id])
  end

  def new
    # create form
    @materials = Material.all
  end

  def receive
    # update form
    @entry = Entry.find(params[:id])
  end

  def update
    @entry = Entry.find(params[:id])
    @entry.update(receive_date: params[:entry][:receive_date],
                  user_receive: current_user)

    update_material_entry(@entry)
    save(@entry, 'Atualizado com sucesso!', :receive)
  end

  def create
    @materials = Material.all
    entry = Entry.new(requested_date: params[:entry][:requested_date],
                      requested_user: current_user)
    create_material_entry(entry, (params[:material][:id].as_json.size - 1))
    save(entry, 'Cadastrado com sucesso!', :new)
  end

  private

  def create_material_entry(entry, maximum)
    (0..maximum).each do |id|
      entry.material_entry
           .build(requested_amount: params[:material][:amount][id.to_s],
                  material_id: params[:material][:id][id.to_s])
    end
  end

  def update_material_entry(entry)
    (0..(params[:material][:id].as_json.size - 1)).each do |id|
      entry.material_entry[id]
           .amount_received = params[:material][:amount][id.to_s]
    end
  end

  def save(entry, message, location)
    if entry.save
      redirect_to entries_index_path, flash: { success: message }
    else
      ocurred_errors(entry)
      render location
    end
  end
end
