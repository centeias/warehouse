# controller class to manager leave and leave_material models, only:
# Create and View
class LeavesController < ApplicationController
  def index
    @leaves = Leave.all
  end

  def new
    # create form
    @materials = Material.all
    @sector = RequestingSector.all
  end

  def show
    @leave = Leave.find(params[:id])
  end

  def create_sector
    sector = RequestingSector.new(name: params[:sector_name])
    if sector.save
      render json: RequestingSector.all
    else
      render json: nil
    end
  end

  def create
    # Create new leave
    @sector = RequestingSector.all
    @materials = Material.all
    leave = new_leave
    create_material_leave(leave, (params[:material][:id].as_json.size - 1))
    save(leave, 'Cadastrado com sucesso!', :new)
  end

  private

  def create_material_leave(leave, maximum)
    (0..maximum).each do |id|
      leave.material_leave
           .build(amount_received: params[:material][:amount][id.to_s],
                  material_id: params[:material][:id][id.to_s])
    end
  end

  def new_leave
    Leave.new(requested_date: params[:leave][:requested_date],
              employee: current_user,
              requested_person: params[:leave][:requested_person],
              requesting_sector_id:  params[:leave][:sector])
  end

  def save(leave, message, location)
    if leave.save
      redirect_to leaves_index_path, flash: { success: message }
    else
      ocurred_errors(leave)
      render location
    end
  end
end
