# controller class to manager CRUD user models
class UsersController < ApplicationController
  before_action :autenticated?

  def index
    @users = User.all
  end

  def active
    if User.exists?(params[:id])
      user = User.find(params[:id])
      user.update_attributes(active: 1)

      message = 'Ativado com sucesso!'
      redirect_to users_index_path, flash: { success: message }
    else
      message = 'Usuário não encontrado'
      redirect_to users_index_path, flash: { error: message }
    end
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      message = 'Cadastrado com sucesso!'
      redirect_to users_index_path, flash: { success: message }
    else
      ocurred_errors(@user)
      render :new
    end
  end

  def update
    @user = current_user
    if @user.update(user_params)
      message = 'Dados atualizados com sucesso'
      redirect_to users_index_path, flash: { success: message }
    else
      ocurred_errors(@user)
      render :edit
    end
  end

  def edit
    @user = current_user
  end

  def destroy
    if User.all.where(active: 1).size != 1
      current_user.update_attributes(active: 0)
      session[:user_id] = nil
      redirect_to log_in_path, flash: { success: 'Usuário desativado' }
    else
      message = 'Impossível desativar último usuário do sistema'
      redirect_to home_path, flash: { error: message } if User.all.size == 1
    end
  end

  private

  def user_params
    params[:user].permit(:email, :password, :password_confirmation)
  end
end
