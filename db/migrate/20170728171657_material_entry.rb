class MaterialEntry < ActiveRecord::Migration[5.0]
  def change
    create_table :material_entries do |t|
      t.integer :requested_amount
      t.integer :amount_received
      t.references :material, foreign_key:true
      t.references :entry, foreign_key:true

      t.timestamps
    end
  end
end
