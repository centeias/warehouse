class CreateEntries < ActiveRecord::Migration[5.0]
  def change
    create_table :entries do |t|
      t.date :requested_date
      t.date :receive_date
      t.references :requested_user, foreign_key:true
      t.references :user_receive, foreign_key:true

      t.timestamps
    end
  end
end
