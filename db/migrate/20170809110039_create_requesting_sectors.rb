class CreateRequestingSectors < ActiveRecord::Migration[5.0]
  def change
    create_table :requesting_sectors do |t|
      t.string :name

      t.timestamps
    end
  end
end
