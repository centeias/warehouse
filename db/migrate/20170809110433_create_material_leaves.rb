class CreateMaterialLeaves < ActiveRecord::Migration[5.0]
  def change
    create_table :material_leaves do |t|
      t.integer :amount_received
      t.references :material, foreign_key: true
      t.references :leave, foreign_key: true

      t.timestamps
    end
  end
end
