class CreateLeaves < ActiveRecord::Migration[5.0]
  def change
    create_table :leaves do |t|
      t.date :requested_date
      t.references :employee, foreign_key: true
      t.string :requested_person
      t.references :requesting_sector, foreign_key: true

      t.timestamps
    end
  end
end
