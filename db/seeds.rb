# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

user_1 = User.create(email: 'admin@admin.com', password: '123456', password_confirmation: '123456')
user_2 = User.create(email: 'admin1@admin.com', password: '123456', password_confirmation: '123456')

material_1 = Material.create(name: 'Alcool', initial_amount: 50)
material_2 = Material.create(name: 'Resma de papel', initial_amount: 0)
