require 'rails_helper'

RSpec.describe HomeController, type: :controller do
  describe 'Try load home page' do
    it 'shoud redirect from home page, if try load unless logged in' do
      get  :index
      expect(response).to have_http_status(302)
    end

    it 'shoud redirect from home page' do
      @user = User.create(email: 'test.admin@admin.com',
                          password: '123456',
                          password_confirmation: '123456')
      session[:user_id] = @user.id
      get :index
      expect(response).to have_http_status(200)
    end
  end
end
