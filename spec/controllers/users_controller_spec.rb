require 'rails_helper'

RSpec.describe UsersController, type: :controller do
  describe 'CRUD of logged user' do
    before(:each) do
      @user = User.create(email: 'test.admin@admin.com',
                          password: '123456',
                          password_confirmation: '123456')
      session[:user_id] = @user.id
    end

    it 'should render register page logged' do
      get :new
      expect(response).to have_http_status(200)
    end

    it 'should render index page logged' do
      get :index
      expect(response).to have_http_status(200)
    end


    it 'should render edit page logged' do
      get :edit
      expect(response).to have_http_status(200)
    end

    it 'should create new user' do
      post :create, params: {user: { email: 'test1.admin@admin.com',
                                     password: '123456',
                                     password_confirmation: '123456' } }
      expect(flash[:success]).to eq('Cadastrado com sucesso!')
    end

    it 'should create new user with invalid data' do
      post :create, params: {user: { email: '',
                                     password: '123456',
                                     password_confirmation: '123456' } }
      expect(flash[:error]).to eq('Preencha o Email')
    end

    it 'should update new user' do
      post :update, params: {user: { email: 'test1.admin@admin.com',
                                     password: '123456',
                                     password_confirmation: '123456' } }
      expect(flash[:success]).to eq('Dados atualizados com sucesso')
    end

    it 'should update new user with invalid data' do
      post :update, params: {user: { email: '',
                                     password: '123456',
                                     password_confirmation: '123456' } }
      expect(flash[:error]).to eq('Preencha o Email')
    end

    it 'should delete unique user account' do
      get :destroy
      expect(flash[:error]).to eq('Impossível desativar último usuário do sistema')
      expect(@user.active).to eq(1)
    end

    it 'should delete user account' do
      User.create(email: 'test1.admin@admin.com',
                  password: '123456',
                  password_confirmation: '123456')
      user_id = User.find(session[:user_id]).id
      get :destroy
      expect(flash[:success]).to eq('Usuário desativado')
      expect(User.find(user_id).active).to eq(0)
    end

    it 'should active user account' do
      user = User.create(email: 'test1.admin@admin.com',
                         password: '123456',
                         password_confirmation: '123456',
                         active: 0)
      get :active, params: {id: user.id}
      expect(flash[:success]).to eq('Ativado com sucesso!')
      expect(User.find(user.id).active).to eq(1)
    end

    it 'should active invalid user account' do
      get :active, params: {id: 10}
      expect(flash[:error]).to eq('Usuário não encontrado')
    end
  end

  describe 'autenticate tests' do

    it 'should render register page without logged' do
      get :new
      expect(response).to have_http_status(302)
    end

    it 'should render edit page without logged' do
      get :edit
      expect(response).to have_http_status(302)
    end
  end
end
