require 'rails_helper'

RSpec.describe SessionsController, type: :controller do
  describe 'login tests' do
    before(:each) do
      @user = User.create(email: 'test.admin@admin.com',
                          password: '123456',
                          password_confirmation: '123456')

      @user_not_active = User.create(email: 'test1.admin@admin.com',
                                    password: '123456',
                                    password_confirmation: '123456',
                                    active: 0)
    end

    it 'should render login page' do
      get :new
      expect(response).to have_http_status(200)
    end

    it 'should login in system' do
      post :create, params: { email: 'test.admin@admin.com',
                              password: '123456' }
      expect(flash[:notice]).to eq('Logged in!')
    end

    it 'should login in system with invalid email' do
        post :create, params: { email: 'test.admin1@admin.com',
                                password: '123456' }
        expect(flash[:error]).to include('Usuário e/ou senha inválidos')
    end

    it 'should login in system with invalid password' do
        post :create, params: { email: 'test.admin@admin.com',
                                password: '1234561' }
        expect(flash[:error]).to include('Usuário e/ou senha inválidos')
    end

    it 'should login in system with inactive user' do
        post :create, params: { email: 'test1.admin@admin.com',
                                password: '123456' }
        expect(flash[:error]).to include('Usuário desativado')
    end

    it 'should logout system' do
      post :create, params: { email: 'test.admin@admin.com',
                              password: '123456' }
      get :destroy
      expect(flash[:notice]).to eq('Logged out!')
    end
  end
end
