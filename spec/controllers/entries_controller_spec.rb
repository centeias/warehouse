require 'rails_helper'

RSpec.describe EntriesController, type: :controller do
  describe 'open entry pages' do
    before(:each) do
      @user = User.create(email: 'test.admin@admin.com',
                          password: '123456',
                          password_confirmation: '123456')
      session[:user_id] = @user.id
      @material = Material.create(name: 'caneta',
                                  initial_amount: 50)
      @entry = Entry.new(requested_date: Date.today,
                         requested_user: @user)

      @entry.material_entry
            .build(requested_amount: 50,
                   material: @material)
      @entry.save
    end

    it 'shoud open index view' do
      get :index
      expect(response).to have_http_status(200)
    end

    it 'shoud open show view' do
      get :show, params: {id: @entry.id}
      expect(response).to have_http_status(200)
    end

    it 'shoud open new view' do
      get :new
      expect(response).to have_http_status(200)
    end

    it 'shoud open receive view' do
      get :receive, params: {id: @entry.id}
      expect(response).to have_http_status(200)
    end
  end

  describe 'try create and update' do
    before(:each) do
      @user = User.create(email: 'test.admin@admin.com',
                          password: '123456',
                          password_confirmation: '123456')
      session[:user_id] = @user.id
      @material = Material.create(name: 'caneta',
                                  initial_amount: 50)
      @entry = Entry.new(requested_date: Date.today,
                         requested_user: @user)

      @entry.material_entry
            .build(requested_amount: 50,
                   material: @material)
      @entry.save
    end

    it 'should save new entry' do
      post :create, params: {entry: {requested_date: '2018-01-01 10:00:00'},
                             material: {id: {'0': '1'}, amount: {'0': '50'}}}
      expect(flash[:success]).to eq('Cadastrado com sucesso!')
    end

    it 'should save new without material' do
      post :create, params: {entry: {requested_date: '2018-01-01 10:00:00'},
                             material: {id: {'0': '1'}, amount: {'0': ''}}}
      expect(flash[:error]).to include('Quantidade solicitada deve ser preenchida')
    end

    it 'should update new entry' do
      post :update, params: {id: @entry.id,
                             entry: {receive_date: '2018-01-01 15:00:00'},
                             material: {id: {'0': '1'}, amount: {'0': '10'}}}
      expect(flash[:success]).to eq('Atualizado com sucesso!')
    end

    it 'should update without entry' do
      post :update, params: {id: @entry.id,
                             entry: {receive_date: '2018-01-01 15:00:00'},
                             material: {id: {'0': '1'}, amount: {'0': ''}}}
      expect(flash[:error]).to eq('Quantidade recebida deve ser preenchida')
    end
  end
end
