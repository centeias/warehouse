require 'rails_helper'
require 'sessions_controller'

RSpec.describe UsersHelper, type: :helper do

  describe 'Users amount' do
    it 'shound get all users in system database' do
      User.create(email: 'test.admin@admin.com',
                  password: '123456',
                  password_confirmation: '123456')

      User.create(email: 'test1.admin@admin.com',
                  password: '123456',
                  password_confirmation: '123456')
      User.create(email: 'test2.admin@admin.com',
                  password: '123456',
                  password_confirmation: '123456')
      expect(total_users).to eq(3)
    end
  end
end
