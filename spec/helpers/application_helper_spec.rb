require 'rails_helper'
require 'sessions_controller'

RSpec.describe ApplicationHelper, type: :helper do

  describe 'Testing ApplicationHelper methods' do
    it 'Show return notice message' do
      flash[:notice] = 'Message Test'
      expect(flash_message).to eq('Message Test')
    end

    it 'Show return info message' do
      flash[:info] = 'Message Test'
      expect(flash_message).to eq('Message Test')
    end

    it 'Show return warning message' do
      flash[:warning] = 'Message Test'
      expect(flash_message).to eq('Message Test')
    end

    it 'Show return error message' do
      flash[:error] = 'Message Test'
      expect(flash_message).to eq('Message Test')
    end
  end
end
