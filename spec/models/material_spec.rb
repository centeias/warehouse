require 'rails_helper'

RSpec.describe Material, :type => :model do
  it 'create valid material' do
    material = Material.create(name: 'Alcool', initial_amount: 0)
    expect(material).to be_valid
  end

  it 'create invalid name' do
    material = Material.create(initial_amount: 0)
    expect(material).to_not be_valid
  end
  it 'create invalid amount' do
    material = Material.create(name: 'Alcool')
    expect(material).to_not be_valid
  end
  it 'create amount not numericality' do
    material = Material.create(name: 'Alcool', initial_amount: 'aa')
    expect(material).to_not be_valid
  end
end
