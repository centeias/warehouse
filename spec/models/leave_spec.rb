require 'rails_helper'

RSpec.describe Leave, type: :model do

  describe 'Leave test' do
    before(:each) do
      @user = User.create(email: 'test.admin@admin.com',
                         password: '123456',
                         password_confirmation: '123456')
      @sector = RequestingSector.create(name: 'Almoxarifado')
    end

    it 'create valid leave' do
      leave = Leave.create(requested_person: 'Joao',
                           employee: @user,
                           requested_date: '2018-05-18 10:00:00',
                           requesting_sector: @sector)

      expect(leave).to be_valid
    end

    it 'create invalid employee' do
      leave = Leave.create(requested_person: 'Joao',
                           requested_date: '2018-05-18 10:00:00',
                           requesting_sector: @sector)

      expect(leave).to_not be_valid
    end

    it 'create invalid requested date' do
      leave = Leave.create(requested_person: 'Joao',
                           employee: @user,
                           requesting_sector: @sector)

      expect(leave).to_not be_valid
    end

    it 'create invalid requested sector' do
      leave = Leave.create(requested_person: 'Joao',
                           employee: @user,
                           requested_date: '2018-05-18 10:00:00')

      expect(leave).to_not be_valid
    end
  end
end
