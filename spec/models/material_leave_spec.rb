require 'rails_helper'

RSpec.describe MaterialLeave, type: :model do
  describe 'Material Leave Associations' do
    before(:each) do
      @user = User.create(email: 'test.admin@admin.com',
                         password: '123456',
                         password_confirmation: '123456')
      @sector = RequestingSector.create(name: 'Almoxarifado')
      @material = Material.create(name: 'Alcool', initial_amount: 0)
    end

    it 'leave with valid materials' do
      leave = Leave.new(requested_person: 'Joao',
                        employee: @user,
                        requested_date: '2018-05-18 10:00:00',
                        requesting_sector: @sector)
      leave.material_leave.build(amount_received: 5,
                                 material: @material)
      leave.save
      expect(leave).to be_valid
    end

    it 'leave with invalid amount received' do
      leave = Leave.new(requested_person: 'Joao',
                        employee: @user,
                        requested_date: '2018-05-18 10:00:00',
                        requesting_sector: @sector)
      leave.material_leave.build(material: @material)
      leave.save
      expect(leave).to_not be_valid
    end

    it 'leave with no numerically amount received' do
      leave = Leave.new(requested_person: 'Joao',
                        employee: @user,
                        requested_date: '2018-05-18 10:00:00',
                        requesting_sector: @sector)
      leave.material_leave.build(amount_received: 'a',
                                 material: @material)
      leave.save
      expect(leave).to_not be_valid
    end
  end
end
