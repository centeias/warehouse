require 'rails_helper'

RSpec.describe User, :type => :model do
  it 'create valid user' do
    user = User.create(email: 'test.admin@admin.com',
                       password: '123456',
                       password_confirmation: '123456')
    expect(user).to be_valid
  end

  it 'create diferents passwords' do
    user = User.create(email: 'test.admin@admin.com',
                       password: '123456',
                       password_confirmation: '123465')
    expect(user).to_not be_valid
  end

  it 'create blanck password' do
    user = User.create(email: 'test.admin@admin.com',
                       password: '',
                       password_confirmation: '')
    expect(user).to_not be_valid
  end

  it 'create blanck email' do
    user = User.create(email: '',
                       password: '123456',
                       password_confirmation: '123456')
    expect(user).to_not be_valid
  end

  it 'create existent email' do
    user_1 = User.create(email: 'test.admin@admin.com',
                       password: '123456',
                       password_confirmation: '123456')

    user = User.create(email: 'test.admin@admin.com',
                       password: '123456',
                       password_confirmation: '123456')
    expect(user).to_not be_valid
  end

  it 'should login' do
    user = User.create(email: 'test.admin@admin.com',
                       password: '123456',
                       password_confirmation: '123456')
    expect(User.authenticate('test.admin@admin.com', '123456')).to_not be_nil
  end

  it 'should login with wrong data' do
    user = User.create(email: 'test.admin@admin.com',
                       password: '123456',
                       password_confirmation: '123456')
    expect(User.authenticate('admin@admin.com', '123456')).to be_nil
  end
end
